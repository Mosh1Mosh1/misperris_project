from django.shortcuts import render
from django.http import HttpResponse
from .models import Usuario, Rescatado
from django.shortcuts import redirect
from django.contrib.auth.models import User, Group
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib.auth.decorators import login_required

#rest framework
from rest_framework import viewsets
from .serializers import UsuarioSerializer, RescatadoSerializer, SocialSerializer

#django allauth
from allauth.socialaccount.models import SocialAccount

# Create your views here.
def index(request):
    return render(
        request,
        'index.html',
        {
            'usuarios': Usuario.objects.all()
        }
    )

def iniciar_sesion(request):
    username = request.POST.get('username', '')
    password = request.POST.get('passwordlogin', '')
    user = authenticate(request, username = username, password = password)

    if user is not None:
        auth_login(request, user)
        return redirect('index')
    else:
        return redirect('login')

class SocialView(viewsets.ModelViewSet):
    queryset = SocialAccount.objects.all()
    serializer_class = SocialSerializer

def profile(request):
    return render(
        request,
        'profile.html',
        {
            'usuarios': Usuario.objects.all()
        }
    )

def cerrar_sesion(request):
    logout(request)
    return redirect('index')

def newpassword(request):
    return render(
        request,
        'newpassword.html',
        {
            'usuarios': Usuario.objects.all()
        }
    )

def newpassword_nolog(request):
    username = request.POST.get('username', None)

    try:
        usuario = Usuario.objects.get(usuario = username)
    except Usuario.DoesNotExist:
        usuario = None

    return render(
        request,
        'newpassword_nolog.html',
        {
            'usuario': usuario
        }
    )

def changepass(request):
    username = request.POST.get('username', '')
    respuesta = request.POST.get('respuesta', '')
    newpassword = request.POST.get('newpassword', '')

    #obtener usuario
    try:
        usuario = Usuario.objects.get(usuario = username)
    except Usuario.DoesNotExist:
        usuario = None

    if usuario is not None:
        #obtener usuario django
        try:
            user = User.objects.get(username = username)
        except User.DoesNotExist:
            user = None

        #comprobar respuesta secreta
        if usuario.respuesta == respuesta:
            #cambiar contraseña
            try:
                usuario.userpassword = newpassword
                usuario.save()
                user.set_password(newpassword)
                user.save()
            except:
                print("error al cambiar contraseña")
                
            return redirect('index')
        else:
            return redirect('login')
    else:
        return redirect('login')

def facebook_login(request):
    return None

class UsuariosView(viewsets.ModelViewSet):
    queryset = Usuario.objects.all()
    serializer_class = UsuarioSerializer

def agregar_usuario(request):
    message = ""

    run = request.POST.get('run', '')
    nombre = request.POST.get('nombre', '')
    nacimiento = request.POST.get('nacimiento', '')
    telefono = request.POST.get('telefono', 0)
    correo = request.POST.get('correo', '')
    username = request.POST.get('username', '')
    userpassword = request.POST.get('password', '')
    avatar = request.FILES.get('avatar', 'avatar/default.png')
    pregunta = request.POST.get('pregunta', '')
    respuesta = request.POST.get('respuesta', '')
    region = request.POST.get('region', '')
    comuna = request.POST.get('comuna', '')
    vivienda = request.POST.get('vivienda', '')

    usuario = Usuario(
        correo = correo,
        run = run,
        nombre = nombre,
        nacimiento = nacimiento,
        telefono = telefono,
        usuario = username,
        password = userpassword,
        avatar = avatar,
        pregunta = pregunta,
        respuesta = respuesta,
        region = region,
        comuna = comuna,
        vivienda = vivienda
    )
    #print(correo,run,nombre,nacimiento,telefono,usuario,userpassword,avatar,pregunta,respuesta,region,comuna,vivienda)
    usuario.save()

    user = User.objects.create_user(username, correo, userpassword)
    user.save()

    if user is not None:
        message = "Registro correcto"
    else:
        message = "Registro incorrecto"
    
    return HttpResponse(message)

def agregar_usuario_social(request):
    message = ""

    run = request.POST.get('run', '')
    nombre = request.POST.get('nombre', '')
    nacimiento = request.POST.get('nacimiento', '')
    telefono = request.POST.get('telefono', 0)
    correo = request.POST.get('correo', '')
    username = request.POST.get('username', '')
    userpassword = request.POST.get('password', '')
    avatar = request.FILES.get('avatar', 'avatar/default.png')
    pregunta = request.POST.get('pregunta', '')
    respuesta = request.POST.get('respuesta', '')
    region = request.POST.get('region', '')
    comuna = request.POST.get('comuna', '')
    vivienda = request.POST.get('vivienda', '')

    usuario = Usuario(
        correo = correo,
        run = run,
        nombre = nombre,
        nacimiento = nacimiento,
        telefono = telefono,
        usuario = username,
        password = userpassword,
        avatar = avatar,
        pregunta = pregunta,
        respuesta = respuesta,
        region = region,
        comuna = comuna,
        vivienda = vivienda
    )
    # print(correo,run,nombre,nacimiento,telefono,usuario,userpassword,avatar,pregunta,respuesta,region,comuna,vivienda)
    usuario.save()

    if usuario is not None:
        message = "Registro correcto"
    else:
        message = "Registro incorrecto"
    
    return HttpResponse(message)

class RescatadosView(viewsets.ModelViewSet):
    queryset = Rescatado.objects.all()
    serializer_class = RescatadoSerializer

def rescued(request):
    return render(
        request,
        'rescued.html',
        {
            'usuarios': Usuario.objects.all(),
            'rescatados': Rescatado.objects.all()
        }
    )

def agregar_rescued(request):
    message = ""

    nombre = request.POST.get('nombrerescued', '')
    raza = request.POST.get('raza', '')
    foto = request.FILES.get('foto', False)
    descripcion = request.POST.get('descripcion', '')
    estado = request.POST.get('estado', '')

    rescatado = Rescatado(
        nombre = nombre,
        raza = raza,
        foto = foto,
        descripcion = descripcion,
        estado = estado
    )
    rescatado.save()

    if rescatado is not None:
        message = "Registro de rescatado correcto"
    else:
        message = "Registro de rescatado incorrecto"

    return HttpResponse(message)

def editar_rescued(request):
    id = request.POST.get('id', 0)
    nombre = request.POST.get('nombrerescued', '')
    raza = request.POST.get('raza', '')
    foto = request.FILES.get('foto', False)
    descripcion = request.POST.get('descripcion', '')
    estado = request.POST.get('estado', '')

    rescatado = Rescatado.objects.get(pk = id)

    rescatado.nombre = nombre
    rescatado.raza = raza
    rescatado.foto = foto
    rescatado.descripcion = descripcion
    rescatado.estado = estado
    rescatado.save()

    return redirect('rescued')

def eliminar_rescued(request, id):
    rescatado = Rescatado.objects.get(pk = id)
    rescatado.delete()

    return redirect('rescued')

def login(request):
    return render(
        request,
        'login.html',
        {
            'usuarios': Usuario.objects.all()
        }
    )

def galeria(request):
    return render(
        request,
        'galeria.html',
        {
            'usuarios': Usuario.objects.all(),
            'rescatados': Rescatado.objects.all()
        }
    )

def pwa(request):
    return render(
        request,
        'pwa.html',
        {
            'usuarios': Usuario.objects.all()
        }
    )

def index_pwa(request):
    return render(
        request,
        'index_pwa.html',
        {
            'usuarios': Usuario.objects.all()
        }
    )

def registro_pwa(request):
    return render(
        request,
        'registro_pwa.html',
        {
            'usuarios': Usuario.objects.all()
        }
    )

def rescatados_pwa(request):
    return render(
        request,
        'rescatados_pwa.html',
        {
            'usuarios': Usuario.objects.all()
        }
    )

def galeria_pwa(request):
    return render(
        request,
        'galeria_pwa.html',
        {
            'usuarios': Usuario.objects.all()
        }
    )

def change_superuser(request):
    user = User.objects.get(username = 'admin')
    user.is_superuser = True
    user.save()

    return redirect('index')