var CACHE_NAME = 'misperris-cache-v1';
var urlsToCache = [
    '/pwa/',
    '/static/css/bootstrap.css',
    '/static/css/bootstrap.css.map',
    '/static/css/bootstrap-grid.css',
    '/static/css/bootstrap-grid.css.map',
    '/static/css/bootstrap-grid.min.css',
    '/static/css/bootstrap-grid.min.css.map',
    '/static/css/bootstrap.min.css',
    '/static/css/bootstrap.min.css.map',
    '/static/css/bootstrap-reboot.css',
    '/static/css/bootstrap-reboot.css.map',
    '/static/css/bootstrap-reboot.min.css',
    '/static/css/bootstrap-reboot.min.css.map',
    '/static/css/style.css',
    '/static/js/app.js',
    '/static/js/app_pwa.js',
    '/static/js/bootstrap.bundle.js',
    '/static/js/bootstrap.bundle.js.map',
    '/static/js/bootstrap.bundle.min.js',
    '/static/js/bootstrap.bundle.min.js.map',
    '/static/js/bootstrap.js',
    '/static/js/bootstrap.js.map',
    '/static/js/bootstrap.min.js',
    '/static/js/bootstrap.min.js.map',
    '/static/js/jquery-3.3.1.js',
    '/static/js/jquery.validate.js',
    '/static/js/popper.js',
    '/static/img/etc/logo.png',
    '/static/img/pwa-background.jpg'
];

self.addEventListener('install', function(event) {
    // Perform install steps
    event.waitUntil(
        caches.open(CACHE_NAME)
        .then(function(cache) {
            console.log('Cache open!');
            return cache.addAll(urlsToCache);
        })
    );
});


self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.match(event.request)
        .then(function(response) {
            // Cache hit - return response
            if (response) {
                return response;
            }
            return fetch(event.request);
        })
    );
});

// self.addEventListener('activate', event => {
//     // remove old caches
//     event.waitUntil(
//       caches.keys().then(keys => Promise.all(
//         keys.map(key => {
//           if (key != CACHE_NAME) {
//             return caches.delete(key);
//           }
//         })
//       )).then(() => {
//         console.log('Now ready to handle fetches!');
//       })
//     );
// });


