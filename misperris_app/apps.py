from django.apps import AppConfig


class MisperrisAppConfig(AppConfig):
    name = 'misperris_app'
