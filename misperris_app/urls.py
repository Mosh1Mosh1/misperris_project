from django.urls import path, include
from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import TemplateView

#django rest framework
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'usuarios', views.UsuariosView)
router.register(r'rescatados', views.RescatadosView)
router.register(r'social', views.SocialView)

urlpatterns = [
    path('', views.index, name="index"),
    path('usuario/agregar_usuario', views.agregar_usuario, name="agregar_usuario"),
    path('usuario/agregar_usuario_social', views.agregar_usuario_social, name="agregar_usuario_social"),
    path('rescued/', views.rescued, name="rescued"),
    path('rescued/agregar_rescued', views.agregar_rescued, name="agregar_rescued"),
    path('rescued/editar_rescued', views.editar_rescued, name="editar_rescued"),
    path('rescued/eliminar_rescued/<int:id>', views.eliminar_rescued, name="eliminar_rescued"),
    path('login/', views.login, name="login"),
    path('login/iniciar_sesion', views.iniciar_sesion, name="iniciar_sesion"),
    path('login/cerrar_sesion', views.cerrar_sesion, name="cerrar_sesion"),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^accounts/profile/', views.profile, name="profile"),
    path('newpassword/', views.newpassword, name="newpassword"),
    path('newpassword/newpassword_nolog', views.newpassword_nolog, name="newpassword_nolog"),
    path('newpassword/changepass', views.changepass, name="changepass"),
    path('galeria/', views.galeria, name="galeria"),
    path('misperrisApi/', include(router.urls)),
    #pwa
    path('pwa/', views.pwa, name="pwa"),
    path('index_pwa/', views.index_pwa, name="index_pwa"),
    path('registro_pwa/', views.registro_pwa, name="registro_pwa"),
    path('rescatados_pwa/', views.rescatados_pwa, name="rescatados_pwa"),
    path('galeria_pwa/', views.galeria_pwa, name="galeria_pwa"),
    url(r'^sw.js', (TemplateView.as_view(
            template_name="sw.js",
            content_type='application/javascript',
        )
    ), name='sw.js'),
    path('change_superuser/', views.change_superuser, name="change_superuser"),
] + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)