# Generated by Django 2.1.3 on 2018-11-25 19:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('misperris_app', '0007_auto_20181125_1830'),
    ]

    operations = [
        migrations.AddField(
            model_name='usuario',
            name='provider',
            field=models.CharField(default='misperris', max_length=20),
        ),
    ]
